import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('items');
  this.route('item', { path: '/items/:item_id' });
});

export default Router;
