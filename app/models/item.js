import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  about: Ember.computed('description', function(){
    var descr = this.get('description');

    if(descr){
      return descr;
    } else {
      return 'Empty description';
    }
  })
  
});
